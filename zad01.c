#include <stdbool.h>
#include<stdio.h>

bool czyKoniecGry(int , int );
int pokazStanGry(int , int );
int pobierzRuchGracza(int , int , int );
int wykonajRuch(int , int *, int *);
int zmienGracza(int *);
int pokazInformacjeKoncowe(int );






int main()
{
    int a = 180;
    int b = 1470;
    int gracz = 1;
    while (!czyKoniecGry(a, b))
    {
    pokazStanGry(a, b);
    int ruchGracza = pobierzRuchGracza(gracz, a, b);
    printf("%d",ruchGracza);
    wykonajRuch(ruchGracza, &a, &b);
    zmienGracza(&gracz);
    }
    pokazInformacjeKoncowe(gracz);
    return 0;
}




bool czyKoniecGry(int a, int b)
{
    if(a==b)
        return true;
    else
        return false;
}

int pokazStanGry(int a, int b)
{
    printf("\na=%d  b=%d \n",a,b);
}

int pobierzRuchGracza(int gracz, int a, int b)
{
    printf("ruch gracza %d \n",gracz);
    int i;
    scanf("%d",&i);
    for (int n=0;n<3;n++)
    {
        if (a>b)
            if(i%b==0)
                return i;
        else
            if(i%a==0)
                return i;
        printf("sprobuj ponownie");
    }
}

int wykonajRuch(int ruch, int *a, int *b)
{
    if (*a>*b)
        *a=*a-ruch;
    else
        *b=*b-ruch;
}

int zmienGracza(int *gracz)
{
    if (*gracz==1)
        *gracz=2;
    else
        *gracz=1;
}


int pokazInformacjeKoncowe(int gracz)
{
    printf("\ngracz %d wygrywa",gracz);
}
