#!/bin/bash


echo "\documentclass[12pt]{article}"> $2.tex
echo "\begin{document}">>$2.tex

echo "\begin{center}">>$2.tex
echo " \begin{tabular}{|l|}">>$2.tex
echo "  \hline">>$2.tex


echo "     `./sq $1 $3` \\\\ \\hline">>$2.tex

echo "\end{tabular}">>$2.tex
echo "\end{center}">>$2.tex
echo "\end{document}">>$2.tex

pdflatex $2.tex